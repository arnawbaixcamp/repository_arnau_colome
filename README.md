**Pràctica 1. BitBucket**

Canvi per a fer pull

Documenteu tots els passos que feu amb les captures oportunes.
1.    Crear un compte a Bitbucket
Aneu a la pàgina web oficial de Bitbucket i doneu-vos d’alta com a usuaris.

2.    Instal·lar Git a Windows
Seguiu els pasos indicats a

https://support.atlassian.com/bitbucket-cloud/docs/install-and-set-up-git/

 (per fer el segon pas, executeu a c:\Windows\Archivos de programa\Git\git-bash).

3.    Crear un repository a Bitbucket Cloud
Seguiu els pasos indicats a

https://support.atlassian.com/bitbucket-cloud/docs/create-a-repository-in-bitbucket-cloud/

Com a nom del projecte poseu “Projecte Nom_Cognom” i, pel nom del repository, poseu repository_nom_cognom.

Un cop creat el repositori, familiaritzeu-vos amb la finestra que es mostra.

4.    Editar i crear un fitxer i clonar un repository
Primer, visualitzeu el video


A continuació,

Modifiqueu el fitxer Readme i creeu-ne un de nou.

Després, cloneu el repository seguint els passos següents

https://support.atlassian.com/bitbucket-cloud/docs/clone-a-repository/

5.    A) Pujar (Push) canvis a Bitbucket
Editeu el fitxer que heu creat a l’apartat anterior i afegiu-ne un de nou al repositori local. Després, seguiu els pasos

https://support.atlassian.com/bitbucket-cloud/docs/push-code-to-bitbucket/

B) Actualitzar el repositori local (Pull) am els canvis fets a Bitbucket
Afegiu un fitxer al repositori central, modifiqueu-hi algun dels fitxers i actualitzeu el repositori local amb aquests canvis seguint les instruccions de

https://support.atlassian.com/bitbucket-cloud/docs/pull-code-from-bitbucket/